JavaCL

Implementation of a Monte Carlo simulation, using JavaCL and Maven to generate vectors of uniform random numbers and then use a Device's GPU to calculate an equivalent vector of Gaussian random numbers. 


Version 1.0


Summary of Setup (Configuration & Dependencies)

Since this is configured as a maven project, simply use the pom.xml with your IDE to set it up properly with the necessary dependencies. For reference, the two external libraries required (other than JDK) are:

- com.nativelibs4java
- org.apache.commons:commons-math3:3.0

Once Setup is completed, simply run the one source file included ('JavaCL_main.java').

That's it! Any problems or questions, please don't hesitate to let me know.