package javacl;

import com.nativelibs4java.opencl.*;

import org.apache.commons.math3.random.JDKRandomGenerator;
import org.bridj.Pointer;
import static org.bridj.Pointer.allocateFloats;


/**
 * Created by David on 12/29/2014.
 */
public class JavaCL_main {

    public static void main(String[] args){

        // Creating the platform which is out computer.
        CLPlatform clPlatform = JavaCL.listPlatforms()[0];
        // Getting the GPU device
        CLDevice device = clPlatform.listGPUDevices(true)[0];
        // Verifying that we have the GPU device
        System.out.println("*** New device *** ");
        System.out.println("Vendor: " + device.getVendor());
        System.out.println("Name: " + device.getName() );
        System.out.println("Type: " + device.getType() );
        // Let's make a context
        CLContext context = JavaCL.createContext(null, device);
        // Lets make a default FIFO queue.
        CLQueue queue = context.createDefaultQueue();

        // Open/JavaCL code: Read the program sources and compile them :
        String src = "__kernel void fill_in_values(__global float* a, __global float* b, __global float* out_1, __global float* out_2, int n) \n" +
                "{\n" +
                "    int i = get_global_id(0);\n" +
                "    if (i >= n)\n" +
                "        return;\n" +
                "\n" +
                "   float   r = sqrt(-2.0f * log(a[i]));\n" +
                "   float phi = 2 * M_PI_F * (b[i]);\n" +
                "   out_1[i] = r * cos(phi);\n" +
                "   out_2[i] = r * sin(phi);\n" +
                "}";
        CLProgram program = context.createProgram(src);
        program.build();

        // Create kernel to run on Device (GPU)
        CLKernel kernel = program.createKernel("fill_in_values");

        final long tmp = System.currentTimeMillis();        // Note starting point to determine computing time
        final int n = 2000000;                              // Set Batch size of Uniform & Gaussian random numbers

        // Create Input Pointers to run kernel
        final Pointer<Float>
                aPtr = allocateFloats(n),
                bPtr = allocateFloats(n);

        // Creates uniform random number generators for kernel's Input Pointers
        JDKRandomGenerator rg_A = new JDKRandomGenerator();
        JDKRandomGenerator rg_B = new JDKRandomGenerator();

        // Returns next pseudo-random, uniformly distributed float value between 0.0 and 1.0 from random number generator's sequence
        for (int i = 0; i < n; i++) {
            aPtr.set(i, (float)rg_A.nextFloat());
            bPtr.set(i, (float)rg_B.nextFloat());
        }

        // Create JavaCL input buffers (using the native memory pointers aPtr and bPtr) :
        CLBuffer<Float>
                a = context.createFloatBuffer(CLMem.Usage.Input, aPtr),
                b = context.createFloatBuffer(CLMem.Usage.Input, bPtr);

        // Create JavaCL output buffers :
        CLBuffer<Float> out_1 = context.createFloatBuffer(CLMem.Usage.Output, n);
        CLBuffer<Float> out_2 = context.createFloatBuffer(CLMem.Usage.Output, n);

        // Set kernel's arguments for input/output & computation
        kernel.setArgs(a, b, out_1, out_2, n);

        // Run the kernel
        CLEvent event = kernel.enqueueNDRange(queue, new int[]{n});
        event.invokeUponCompletion(new Runnable() {
            @Override
            public void run() {
                System.out.println((System.currentTimeMillis() - tmp));
            }
        });

        // Read results off the queue via the output buffers & Assign Gaussian random numbers to Output Pointers
        final Pointer<Float> cPtr = out_1.read(queue,event);
        final Pointer<Float> dPtr = out_2.read(queue,event);

        for (int i = 0; i <= n; ++i)    // Iterate through Uniform & Gaussian random number vectors
            if(i%500==499)              // Print example of Vector A (Uniform random) & Vector C (Gaussian random) after every 500 numbers
                System.out.println( aPtr.get(i) + " " + cPtr.get(i) );

        System.out.println();
        System.out.println("Elapsed Time (mils): " + (System.currentTimeMillis() - tmp));   // Display computation time
    }
}
